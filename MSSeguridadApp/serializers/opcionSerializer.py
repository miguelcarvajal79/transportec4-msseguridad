from MSSeguridadApp.models.opcion import Opcion
from rest_framework import serializers

class OpcionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Opcion
        fields = ['id','nombre','estado']