from MSSeguridadApp.models.usuario import Usuario
from rest_framework import serializers

class UsuarioSerializaer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['id', 'username','password','tipoidentificacion','nombre','apellido','celular','email','rolid','estado']