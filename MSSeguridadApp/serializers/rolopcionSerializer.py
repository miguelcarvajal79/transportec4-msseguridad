from MSSeguridadApp.models.rolopcion import RolOpcion
from rest_framework import serializers

class RolOpcionSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolOpcion
        fields = ['rolid','opcionid']