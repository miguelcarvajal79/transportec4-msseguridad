from django.apps import AppConfig


class MsseguridadappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'MSSeguridadApp'
