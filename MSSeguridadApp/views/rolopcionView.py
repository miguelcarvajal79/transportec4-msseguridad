from django.views.generic.base import View
from rest_framework import generics, status 
from rest_framework import views
from rest_framework import response
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from MSSeguridadApp.models.rolopcion import RolOpcion
from MSSeguridadApp.models.rol import Rol
from MSSeguridadApp.models.opcion import Opcion
from MSSeguridadApp.serializers.rolopcionSerializer import RolOpcionSerializer

class RolOpcionView(generics.RetrieveAPIView):
    def get(self, request, pk):
        rolopcion = RolOpcion.objects.get(pk=pk)
        serializer = RolOpcionSerializer(rolopcion)
        
        return Response(serializer.data, 200)

    def post(self, request, *args, **kwargs):
        serializer = RolOpcionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            rol = Rol.objects.get(pk=serializer.validated_data['rolid'])
        except:
            return Response({"Respuesta": "Rol no existe."}, 400)
        try:
            opcion = Opcion.objects.get(pk=serializer.validated_data['opcionid'])
        except:
            return Response({"Respuesta": "Opcion no existe."}, 400)
            
        serializer.save()
        
        return Response(serializer.validated_data, status=status.HTTP_201_CREATED)
    
    def delete(self, request, pk):
        try:
            rolopcion = RolOpcion.objects.get(pk=pk)
            rolopcion.delete()
            return Response({"Respuesta": "RolOpcion eliminada."}, 200)
        except:
            return Response({"Respuesta": "RolOpcion no existe."}, 400)
    
    def put(self, request, pk):
        try:
            rolopcion = RolOpcion.objects.get(pk=pk)
        except:
            return Response({"Respuesta": "RolOpcion no existe"}, 400)
        
        serializer = RolOpcionSerializer(rolopcion, data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            rol = Rol.objects.get(pk=serializer.validated_data['rolid'])
        except:
            return Response({"Respuesta": "Rol no existe."}, 400)
        try:
            opcion = Opcion.objects.get(pk=serializer.validated_data['opcionid'])
        except:
            return Response({"Respuesta": "Opcion no existe."}, 400)
        
        serializer.save()
            
        return Response({"Respuesta": "RolOpcion actualizada."}, 200)
        
        
class RolOpcionAllView(views.APIView):
    def get(self, request):
        listaRolOpciones = RolOpcion.objects.all().order_by('id')
        serializer = RolOpcionSerializer(listaRolOpciones, many=True)
        return Response(serializer.data, 200)