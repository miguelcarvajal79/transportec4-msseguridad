from rest_framework import status, views
from rest_framework.response import Response
from MSSeguridadApp.models.rol import Rol
from MSSeguridadApp.serializers.rolSerializer import RolSerializer
    
class RolView(views.APIView):
    def get(self, request, pk):
        rol = Rol.objects.get(pk=pk)
        serializer = RolSerializer(rol)
        
        return Response(serializer.data, 200)

    def post(self, request, *args, **kwargs):
        serializer = RolSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data['id'], status=status.HTTP_201_CREATED)
    
    def delete(self, request, pk):
        try:
            rol = Rol.objects.get(pk=pk)
            rol.delete()
            return Response({"Respuesta": "Rol eliminado."}, 200)
        except:
            return Response({"Respuesta": "Rol no existe."}, 400)
    
    def put(self, request, pk):
        try:
            rol = Rol.objects.get(pk=pk)
            serializer = RolSerializer(rol, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
                
            return Response({"Respuesta": "Rol actualizado."}, 200)
        except:
            return Response({"Respuesta": "Rol no existe"}, 400)
        
class RolAllView(views.APIView):
    def get(self, request):
        listaRoles = Rol.objects.all().order_by('id')
        serializer = RolSerializer(listaRoles, many=True)
        return Response(serializer.data, 200)
