from django.views.generic.base import View
from rest_framework import generics, status 
from rest_framework import views
from rest_framework import response
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from MSSeguridadApp.models.opcion import Opcion
from MSSeguridadApp.serializers.opcionSerializer import OpcionSerializer

class OpcionView(generics.RetrieveAPIView):
    def get(self, request, pk):
        opcion = Opcion.objects.get(pk=pk)
        serializer = OpcionSerializer(opcion)
        
        return Response(serializer.data, 200)

    def post(self, request, *args, **kwargs):
        serializer = OpcionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data['id'], status=status.HTTP_201_CREATED)
    
    def delete(self, request, pk):
        try:
            opcion = Opcion.objects.get(pk=pk)
            opcion.delete()
            return Response({"Respuesta": "Opcion eliminada."}, 200)
        except:
            return Response({"Respuesta": "Opcion no existe."}, 400)
    
    def put(self, request, pk):
        try:
            opcion = Opcion.objects.get(pk=pk)
            serializer = OpcionSerializer(opcion, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
                
            return Response({"Respuesta": "Opcion actualizada."}, 200)
        except:
            return Response({"Respuesta": "Opcion no existe"}, 400)
        
class OpcionAllView(views.APIView):
    def get(self, request):
        listaOpciones = Opcion.objects.all().order_by('id')
        serializer = OpcionSerializer(listaOpciones, many=True)
        return Response(serializer.data, 200)