from django.db import models
from django.db.models import UniqueConstraint

class RolOpcion(models.Model):
    class Meta:
        UniqueConstraint(fields = ['rolid', 'opcionid'], name = 'PK_RolOpcion')
        
    rolid = models.IntegerField('RolId')
    opcionid =  models.IntegerField('OpcionId')