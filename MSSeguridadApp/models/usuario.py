from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self, id, username, password=None):
        """
        Creates and saves a user with the given username and password.
        """
        if not id:
            raise ValueError('Users must have an identification')
        if not id:
            raise ValueError('Users must have an username')
        usuario = self.model(id = id)
        usuario = self.model(username = username)
        usuario.set_password(password)
        usuario.save(using=self._db)
        return usuario
    
    def create_superuser(self, id, username, password):
        """
        Creates and saves a superuser with the given username and password.
        """
        usuario = self.create_user(
            id = id,
            username = username,
            password = password,
        )
        usuario.is_admin = True
        usuario.save(using=self._db)
        return usuario
    
class Usuario(AbstractBaseUser, PermissionsMixin):
    id = models.IntegerField('Id', primary_key=True)
    username = models.CharField('NombreUsuario', max_length = 15, unique=True)
    password = models.CharField('Clave', max_length = 256)
    tipoidentificacion = models.CharField('TipoIdentificacion', max_length = 2)
    nombre = models.CharField('Nombres', max_length = 50)
    apellido = models.CharField('Apellidos', max_length = 50)
    celular = models.BigIntegerField('Celular')
    email = models.EmailField('Email', max_length = 100)
    rolid = models.IntegerField('RolId',null=True)
    estado = models.CharField('Estado', max_length = 1, default='A')
    
    def save(self, **kwargs):
        some_salt = 'mMUj0IrIG6vgtdUYepkExL'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)
        
    objects = UserManager()
    USERNAME_FIELD = 'username'