from django.db import models
class Rol(models.Model):
    id = models.AutoField('Id', primary_key=True)
    nombre = models.CharField('Nombre', max_length=50)
    estado = models.CharField('Estado', max_length = 1, default='A')