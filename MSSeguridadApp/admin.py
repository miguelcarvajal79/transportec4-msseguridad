from django.contrib import admin
from .models.usuario import Usuario
from .models.rol import Rol
from .models.opcion import Opcion
from .models.rolopcion import RolOpcion

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Rol)
admin.site.register(Opcion)
admin.site.register(RolOpcion)