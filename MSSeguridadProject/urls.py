"""MSSeguridadProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from MSSeguridadApp import views

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('verifyToken/', views.VerifyTokenView.as_view()),
    path('user/', views.UsuarioView.as_view()),
    path('user/<int:pk>/', views.UsuarioView.as_view()),
    path('user/usuarios/', views.UsuarioAllView.as_view()),
    path('rol/', views.RolView.as_view()),
    path('rol/<int:pk>/', views.RolView.as_view()),
    path('rol/roles/', views.RolAllView.as_view()),
    path('opcion/', views.OpcionView.as_view()),
    path('opcion/<int:pk>/', views.OpcionView.as_view()),
    path('opcion/opciones/', views.OpcionAllView.as_view()),
    path('rolopcion/', views.RolOpcionView.as_view()),
    path('rolopcion/<int:pk>/', views.RolOpcionView.as_view()),
    path('rolopcion/rolopciones/', views.RolOpcionAllView.as_view()),
]
